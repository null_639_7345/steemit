module.exports = {
    cwd : 'F:\\test1\\steemit\\',//项目根目录
    redisHost: '192.168.10.6',//redis服务器ip
    redisPost: 6379,
    promulgatorName: '***',//steemit用户名
    promulgatorPassword: "***",//steemit密码
    categories:[
        {
            name: 'popular',//steemit上传文章的分类（tag）
            //Medium的作者列表
            origin: ['joshrose','JessicaLexicus','ThunderPuff','usemuzli','black_metallic']
        }
    ]
};