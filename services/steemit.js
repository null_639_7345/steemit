const { Builder, By, Key, until } = require("selenium-webdriver");
const promiseSynchronizer = require("promise-synchronizer");

class Steemit {
  constructor(name,password) {
    this.web = new Builder().forBrowser("firefox").build();
    this.__username = name;
    this.__password = password;
  }

  /**
   * 登录
   */
  async login() {
    let url = `https://steemit.com/login.html`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise(async (resolve, reject) => {
      await _this.web.get(url);
      await _this.web.findElements(By.css("input")).then(async res => {
        await res[1].sendKeys(_this.__username);//设置登录用户名
        await res[2].sendKeys(_this.__password);//设置登录密码
        await _this.web
          .findElement(By.css(".login-modal-buttons>button"))
          .click();
        await _this.web.wait(until.urlIs("https://steemit.com/welcome"), 4000);
        await _this.web
          .findElement(By.css(".show-for-medium.submit-story"))
          .click();
        returnJson.result = true;
        returnJson.value = "login success";
        console.log("login success");
        resolve(returnJson);
        return returnJson;
      });
    });
  }

  /**
   * 提交文章
   * @param {*} title 文章标题
   * @param {*} post 文章体
   * @param {*} tags 文章标签
   */
  async submit(title, post, tags) {
    let url = `https://steemit.com/submit.html`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise(async (resolve, reject) => {
      await _this.web.get(url);
      //设定延时,确保进入提交页面
      await _this.web.wait(
        until.urlIs("https://steemit.com/submit.html"),
        4000
      );
      //标签定位，打开网页分析一下就oK了
      await _this.web.findElements(By.css("input")).then(async res => {
        await res[1].sendKeys(title);
        await res[3].sendKeys(tags);
        await _this.web.wait(until.elementLocated(By.css("textarea")), 3000);
        await _this.web.findElement(By.css("textarea")).sendKeys(post);
        await _this.web.findElement(By.css(".button")).click();
        returnJson.result = true;
        returnJson.value = "submit success";
        resolve(returnJson);
        return returnJson;
      });
    });
  }
}

export default Steemit;


