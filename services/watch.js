import chokidar from 'chokidar';
import fse from 'fs-extra';
import path from 'path';
import Medium from './medium';
import Queue from './queue';
import promiseSynchronizer from 'promise-synchronizer';
const medium = new Medium();

const config = require('config-lite')(path.join(__dirname,"../"));

class Watch {
    constructor(queue) {
        this.queueJson = {};
        config.categories.forEach(category => {
            this.queueJson[`${category.name}`] = new Queue(category.name,queue);
        });
      }
    async watchOriginDir(originDir) {
        let _this = this;
        let watcher = chokidar.watch(originDir, {
            persistent: true
        });

        // Something to use when events are received.
        let log = console.log.bind(console);
        // Add event listeners.
        watcher
            .on('add', async addpath => {
                log(`File ${addpath} has been added`)
                // 格式化md
                let savePath = addpath.replace(/origin/g, "save");
                // setTimeout(async () => {
                //     await medium.mdFormat(addpath, savePath);
                // }, 10000);
                

 
                // 删除文件 
                // try {
                //     await fse.remove(path)
                //     console.log('success!')
                // } catch (err) {
                //     console.error(err)
                // }
                //写入redis队列中
                // let queueKey = path.basename(path.dirname(path.dirname(savePath)))
                // let postPath = path.basename(savePath,'.md');
                // let postJson = {
                //     title: postPath,
                //     path: savePath
                // }
                // _this.queueJson[queueKey].publish(postJson)
                
                

            })
            .on('unlink', unlinkpath => log(`File ${unlinkpath} has been removed`));


    }

}

export default Watch;
