const rmq = require("redis-message-queue");
const fs = require("fs");
const path = require("path");

class Queue {
  constructor(name, queue) {
    this.name =
      "Steemit:" + name;
    let host = queue.redisHost;
    let port = queue.redisPort;
    this.client = new rmq.UniqueQueue(this.name, port, host);
    this.allQueue = new Array();
  }
  publish(value) {
    this.client.push(JSON.stringify(value), function(err) {
      if (err) {
          console.log(err);
      }
    });
  }
  /**
   * 从相关队列中取出所有的数据
   * 返回：
   *  成功：返回结果集数组
   *  失败：返回失败的原因
   */
  subscribe() {
    let selfThis = this;
    return new Promise(function(resolve, reject) {
      selfThis.client.get(-1, function(err, messages) {
        if (err) {
          console.error(err);
          resolve(err);
        }
        for (let i = 0; i < messages.length; i++) {
          selfThis.allQueue[i] = JSON.parse(messages[i]);
        }
        selfThis.removeQueue();
        resolve(selfThis.allQueue);
      });
    });
  }
  removeQueue() {
    this.client.removeAmount( function(err) {
      if (err) {
        console.log(err);
      }
    });
  }
  removeAllQueue() {
    this.client.removeAmount(-1, function(err) {
      if (err) {
        console.log(err);
      }
    });
  }
  getQueueOne(){
    let selfThis = this;
    let returnJson = {
      result: false,
      value: ''
    }
    return new Promise(function(resolve, reject) {
      selfThis.client.get(function(err, messages) {
        
        if(messages.length){
          returnJson.result = true;
          returnJson.value = messages[0];
          selfThis.removeQueue();
          resolve(returnJson)
          return returnJson;
        }else{
          returnJson.result = false;
          returnJson.value = '从redis队列中读出文章信息失败';
          resolve(returnJson)
          
          return returnJson;
        }
      });
    });
  }
    
  
  
}

module.exports = Queue;
