import superagent from "superagent";
import URL from "url";
import html2markdown from "html2markdown";
import cheerio from "cheerio";
import { spawn,exec } from "child_process";
import path from "path";
import fs from "fs";
import fse from "fs-extra";
import Queue from './queue';
import promiseSynchronizer from 'promise-synchronizer';
const config = require('config-lite')(path.join(__dirname,'..'))
//import readline from 'readline';
let readline = require("fs-readline");

class Medium {
  constructor(queue) {
    this.queueJson = {};
    config.categories.forEach(category => {
        this.queueJson[`${category.name}`] = new Queue(category.name,queue);
    });
  }
  /**
   * 获取Medium文章列表
   * @param {*} user
   * @param {*} limit
   */
  async getPostByUsername(username, limit) {
    let returnJson = {
      result: false,
      value: ""
    };
    let _this = this;
    return new Promise((resolve, reject) => {
      if (
        typeof limit != "number" ||
        limit <= 0 ||
        limit > 100 ||
        typeof username != "string" ||
        username === ""
      ) {
        returnJson.result = false;
        returnJson.value = "参数不正确";
        resolve(returnJson);
        return returnJson;
      } else {
        let JSONDate = {
          query: `query PostQuery($username: String!, $limit: Int!){
                posts(username: $username, limit: $limit) {
                  title
                  firstPublishedAt
                  url
                  content {
                    subtitle
                  }
                }
                user(username: $username) {
                  username
                  name
                  bio
                }
              }`,
          variables: `{
                "username": "${username}",
                "limit": ${limit}
              }`,
          operationName: "PostQuery"
        };

        superagent
          .post("https://micro-medium-api.now.sh/graphql")
          .send(JSONDate) // sends a JSON post body
          .set("accept", "json")
          .end((err, res) => {
            if (err) {
              console.log("获取文章列表出错： ", err);
              returnJson.result = false;
              returnJson.value = "获取文章列表出错： " + err;
              resolve(returnJson);
              return returnJson;
            } else {
              returnJson.result = true;
              returnJson.value = res.text;
              resolve(returnJson);
              return returnJson;
            }
          });
      }
    });
  }

  //html2md convert -u https://medium.com/@evenchange4/react-stack-%E9%96%8B%E7%99%BC%E9%AB%94%E9%A9%97%E8%88%87%E5%84%AA%E5%8C%96%E7%AD%96%E7%95%A5-b056da2fa0aa -o abc1.md -q
  /**
   * 根据url下载Medium文章到本地
   * @param {*} url
   * @param {*} savePath
   * @param {*} saveName
   */
  async downloadPostByUrl(url, saveName,author,category) {
    let returnJson = {
      result: false,
      value: ""
    };
    let _this = this;
    url = encodeURI(url);
    return new Promise((resolve, reject) => {
      if (url === "" || saveName === "" ) {
        returnJson.result = false;
        returnJson.value = "参数不正确";
        resolve(returnJson);
        return returnJson;
      } else {
        let command = `h2m ${url}`;
        console.log(command);
        exec(command, {timeout: 1000*60*3,maxBuffer: 20*1024*1024},(error, stdout, stderr) => {
          console.log(`stderr: ${stderr}`);
          if (error) {
            console.log(`exec error: ${error}`);
            returnJson.result = false;
            returnJson.value = `exec error: ${error}`;
            resolve(returnJson);
          } else {
            returnJson.result = true;
            returnJson.value = `根据url下载Medium文章到本地成功`;
            console.log('根据url下载Medium文章到本地成功');
            resolve(returnJson);
            let saveStr = stdout.slice(stdout.indexOf('\n\n---\n'),stdout.indexOf('One clap, two clap, three clap, forty?'))
            let title = saveName;
            let postJson = {
              title: title,
              author: author,
              category: category,
              content: saveStr
            }
            //存储到redis队列中
            _this.queueJson[category].publish(postJson)
          }
          return returnJson;
        });
      }
    });
  }

  async mdFormat(originFilePath, savePath) {
    let returnJson = {
      result: false,
      value: ""
    };
    
      try {
        await fse.ensureFile(savePath);
        let originStr = fs.readFileSync(originFilePath).toString();
        let saveStr = originStr.slice(originStr.indexOf('\n\n---\n'),originStr.indexOf('One clap, two clap, three clap, forty?'))
        fs.writeFileSync(savePath,saveStr);
      } catch (error) {
        console.log("mdFormat: ", error);
      }
  }

  /**
   * 筛选最近一天的文章
   * @param {*} PostArr
   */
  async screenPost(PostArr) {
    let date = new Date().getTime();
    const oneDay = 24 * 60 * 60 * 1000;
    let resultPosts = [];
    await PostArr.forEach(function(post) {
      if (date - post.firstPublishedAt < oneDay) {
        resultPosts.push(post);
      }
    }, this);
    return resultPosts;
  }



}

export default Medium;
