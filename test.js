const { Builder, By, Key, until } = require("selenium-webdriver");
const superagent = require("superagent");
const promiseSynchronizer = require("promise-synchronizer");

class Spider {
  constructor() {
    this.web = new Builder().forBrowser("firefox").build();
    this.__username = "frank.long";
    this.__password = "P5J9adoYbEw3RcxsJ1peZcENBmsvp1ha3Rg4tuedSpSqy2YbxUek";
    this.headers = {
      accept:
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
      "accept-encoding": "gzip, deflate, br",
      "accept-language": "zh-CN,zh;q=0.8,en;q=0.6",
      "cache-control": "max-age=0",
      "upgrade-insecure-requests": "1",
      "user-agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36"
    };
    this.req = "";
    this.cookies = {};
    this.token = "";
  }

  async login() {
    let url = `https://steemit.com/login.html`;
    let _this = this;

    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise(async (resolve, reject) => {
      _this.web.get(url);
      //_this.web.wait(until.titleIs("Login — Steemit"), 3000);
      _this.web.findElements(By.css("input")).then(async res => {
        await res[1].sendKeys(_this.__username);
        await res[2].sendKeys(_this.__password);
        //_this.web.wait(until.elementLocated(By.css(".button")), 3000);

        //<button type="submit" disabled="" class="button">

        await _this.web
          .findElement(By.css(".login-modal-buttons>button"))
          .click();
        await _this.web.wait(until.urlIs("https://steemit.com/welcome"), 4000);
        await _this.web
          .findElement(By.css(".show-for-medium.submit-story"))
          .click();
        // _this.web.findElements(By.css("input")).then(res1 => {
        //     res1[1].sendKeys('title');
        //     res1[3].sendKeys('tags');
        //     //_this.web.wait(until.elementLocated(By.css("textarea")), 3000);
        //     _this.web.findElement(By.css("textarea")).sendKeys('post');
        //     //selfThis.web.findElement(By.css('.button')).click();login-modal-buttons
        //     returnJson.result = true;
        //     returnJson.value = "submit success";
        //     console.log("submit success");
        //     resolve(returnJson);
        //     return returnJson;
        //   });
        returnJson.result = true;
        returnJson.value = "submit success";
        console.log("submit success");
        resolve(returnJson);
        return returnJson;
        //_this.web.wait(until.titleIs("Login — Steemit"), 3000);
      });
    });
  }

  async submit(title, post, tags) {
    let url = `https://steemit.com/submit.html`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise(async (resolve, reject) => {
      await _this.web.get(url);
      //_this.web.wait(until.titleIs("Welcome — Steemit"), 3000);
      await _this.web.wait(
        until.urlIs("https://steemit.com/submit.html"),
        4000
      );
      await _this.web.findElements(By.css("input")).then(async res => {
        await res[1].sendKeys(title);
        await res[3].sendKeys(tags);
        await _this.web.wait(until.elementLocated(By.css("textarea")), 3000);
        await _this.web.findElement(By.css("textarea")).sendKeys(post);
        await _this.web.findElement(By.css(".button")).click();
        returnJson.result = true;
        returnJson.value = "submit success";
        resolve(returnJson);
        return returnJson;
      });
    });
  }
}

async function start() {
  var spider = new Spider();
  await promiseSynchronizer(spider.login());
  //await promiseSynchronizer(spider.submit("title2", "post2", "tags2"));
    let i = 4;
  //await promiseSynchronizer(spider.submit("title3", "post3", "tags3"));
  setInterval(async () => {
      console.log('次数： ',i);
    i++;
    await promiseSynchronizer(spider.submit("title"+i, "post"+i, "tags"+i));
  }, 1000*60*6);
  
}
start();


