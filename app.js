import promiseSynchronizer from "promise-synchronizer";
import schedule from "node-schedule";
import path from "path";
import fse from "fs-extra";
const config = require("config-lite")(__dirname);

import Medium from "./services/medium";
import Queue from "./services/queue";
import Steemit from "./services/steemit";
const queue = {
  redisHost: config.redisHost,
  redisPort: config.redisPort
};
const medium = new Medium(queue);
const steemit = new Steemit(config.promulgatorName,    promulgatorPassword: "***",
);

let queueJson = [];
config.categories.forEach(category => {
  queueJson[`${category.name}`] = new Queue(category.name, queue);
  queueJson[`${category.name}`].removeAllQueue()
});


let originDir = path.join(config.cwd, "download/origin");

async function start() {

    //每天8点半下载文章，保证下载的文章不重复
//   let downloadPostJob = schedule.scheduleJob(
//     "30 30 8 * *",
//     async function() {}
//   );
  await promiseSynchronizer(steemit.login());

  for (let i = 0; i < config.categories.length; i++) {
    let category = config.categories[i];
    //每隔6分钟上传一篇文章
    let rule = new schedule.RecurrenceRule();
    rule.minute = [0+i%6, 6+i%6, 12+i%6, 18+i%6, 24+i%6, 30+i%6, 36+i%6, 42+i%6, 48+i%6, 54+i%6];
    console.log('uploadPostJob start!!!!!!');
    let uploadPostJob = schedule.scheduleJob(rule, async function() {
      //逐条获取下载文章内容
      let getQueueOneRes = await promiseSynchronizer(queueJson[category.name].getQueueOne());
      if(!getQueueOneRes.result){
        console.log('getQueueOneRes error: ',getQueueOneRes.value)
      }else{
        //console.log('getQueueOneRes',getQueueOneRes.value);
        console.log('*******************************');
        let postTmp = JSON.parse(getQueueOneRes.value.message);
        //console.log(postTmp.title, postTmp.content, postTmp.category)
        //上传文章
        await promiseSynchronizer(steemit.submit(postTmp.title, postTmp.content, postTmp.category));
      }
      
    });
  }
  try {
    config.categories.forEach(function(category) {
      category.origin.forEach(async function(origin) {
        let res = await promiseSynchronizer(
          medium.getPostByUsername(origin, 100)
        );
        if (res.result) {
          let tempList = JSON.parse(res.value);
          //删选文章保证文章是最近一天的
          //let resPosts = await medium.screenPost(tempList.data.posts);

          let resPosts = tempList.data.posts;

          let savePath = path.join(
            config.cwd,
            "download/origin/",
            category.name,
            origin
          );
          //await fse.ensureDir(savePath);

          //开始下载文章
          let author = origin;
          resPosts.forEach(async function(post) {
            let saveName = post.title;
            let downloadPostByUrlRes = await promiseSynchronizer(
              medium.downloadPostByUrl(
                post.url,
                saveName,
                author,
                category.name
              )
            );
          }, this);
        } else {
        }
      }, this);
    }, this);
  } catch (error) {
    console.log(error);
  }
   
}

start();
